# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.wizard import Wizard, StateTransition, StateView, Button
from trytond.transaction import Transaction


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    @classmethod
    def _get_origin(cls):
        models = super(Move, cls)._get_origin()
        models.append('carrier.load.order.line')
        return models

    @property
    def origin_name(self):
        pool = Pool()
        Line = pool.get('carrier.load.order.line')

        name = super(Move, self).origin_name
        if isinstance(self.origin, Line):
            name = self.origin.order.rec_name
        return name


class ShipmentLoadMixin(object):
    __slots__ = ()

    load_orders = fields.One2Many('carrier.load.order', 'shipment',
        'Load orders', readonly=True)
    load = fields.Function(fields.Many2One('carrier.load', 'Load'),
        'get_load', searcher='search_load')

    @classmethod
    def get_load(cls, records, name):
        pool = Pool()
        LoadOrder = pool.get('carrier.load.order')

        res = {r.id: None for r in records}
        orders = LoadOrder.search([
            ('shipment.id', 'in', [r.id for r in records], cls.__name__)])
        for order in orders:
            res[order.shipment.id] = order.load and order.load.id or None
        return res

    @classmethod
    def search_load(cls, name, clause):
        return [('load_orders.%s' % clause[0], ) + tuple(clause[1:])]

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default['load_orders'] = None
        return super(ShipmentLoadMixin, cls).copy(records, default=default)


class ShipmentOut(ShipmentLoadMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.out'

    @classmethod
    def get_carrier_states_methods(cls):
        """
            Returns a list(tuple) of the states ordered by execution.
            does not include cancelled and draft status
            format tuple: ('state', 'method')
        """
        return [
            ('waiting', 'wait'),
            ('assigned', 'assign'),
            ('picked', 'pick'),
            ('packed', 'pack'),
            ('done', 'done'),
        ]


class ShipmentOutReturn(ShipmentLoadMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.out.return'


class ShipmentInternal(ShipmentLoadMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.internal'

    @classmethod
    def get_carrier_states_methods(cls):
        """
            Returns a list(tuple) of the states ordered by execution.
            does not include cancelled, draft, request status
            format tuple: ('state', 'method')
        """
        return [
            ('waiting', 'wait'),
            ('assigned', 'assign'),
            ('shipped', 'ship'),
            ('done', 'done'),
        ]


class ShipmentInReturn(ShipmentLoadMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.in.return'


class LoadStateView(StateView):

    def get_view(self, wizard, state_name):
        with Transaction().set_context(loading_shipment=True):
            return super(LoadStateView, self).get_view(
                wizard, state_name)


class LoadShipment(Wizard):
    """Load Shipment"""
    __name__ = 'stock.shipment.load'

    start = StateTransition()
    load = LoadStateView('carrier.load',
        'carrier_load.load_view_form',
        [Button('Cancel', 'end', 'tryton-cancel'),
         Button('Associate', 'associate', 'tryton-ok')])
    associate = StateTransition()

    def transition_start(self):
        pool = Pool()
        Shipment = pool.get(Transaction().context['active_model'])

        shipments = Shipment.browse(Transaction().context['active_ids'])
        for shipment in shipments:
            if shipment.load:
                return 'end'
        return 'load'

    def default_load(self, fields):
        pool = Pool()
        Shipment = pool.get(Transaction().context['active_model'])
        shipments = Shipment.browse(
            Transaction().context.get('active_ids'))

        return {
            'date': shipments[0].effective_date or shipments[0].planned_date,
            'warehouse': shipments[0].warehouse.id,
            'dock': shipments[0].dock.id if shipments[0].dock else None
        }

    def transition_associate(self):
        pool = Pool()
        LoadOrder = pool.get('carrier.load.order')
        Shipment = pool.get(Transaction().context['active_model'])

        shipments = Shipment.browse(Transaction().context['active_ids'])

        orders = []
        for shipment in shipments:
            order = LoadOrder(
                shipment=shipment,
                load=self.load,
                type=Shipment.__name__[15:].replace('.', '_'),
                state='draft')
            if order.type == 'internal':
                order.to_location = shipment.to_location.id
            elif order.type == 'in_return':
                order.party = shipment.supplier.id
            orders.append(order)
        self.load.orders = orders
        self.load.save()

        return 'end'
