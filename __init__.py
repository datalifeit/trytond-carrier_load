# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import load
from . import configuration
from . import sale
from . import stock
from . import purchase
from . import carrier
from . import party
from . import cmr


def register():
    Pool.register(
        cmr.CMRTemplate,
        cmr.CMRTemplateLine,
        configuration.Configuration,
        carrier.Carrier,
        load.Load,
        load.LoadOrder,
        load.LoadOrderIncoterm,
        load.LoadOrderLine,
        purchase.Purchase,
        stock.Move,
        stock.ShipmentOut,
        stock.ShipmentInternal,
        stock.ShipmentOutReturn,
        stock.ShipmentInReturn,
        configuration.ConfigurationSequence,
        sale.Sale,
        party.Party,
        configuration.ConfigurationStates,
        module='carrier_load', type_='model')
    Pool.register(
        sale.CostType,
        sale.CostTemplate,
        sale.CostSale,
        sale.CostSaleLine,
        load.Load2,
        module='carrier_load', type_='model',
        depends=['sale_cost'])
    Pool.register(
        load.Load3,
        load.LoadOrder3,
        sale.CostSale2,
        module='carrier_load', type_='model',
        depends=['sale_cost_apply_invoice'])
    Pool.register(
        sale.SaleCreditLimit,
        module='carrier_load', type_='model',
        depends=['sale_credit_limit'])
    Pool.register(
        load.Load4,
        module='carrier_load', type_='model',
        depends=['carrier_vehicle'])
    Pool.register(
        load.LoadOrder4,
        module='carrier_load', type_='model',
        depends=['sale_discount'])
    Pool.register(
        load.LoadSheet,
        cmr.CMR,
        load.RoadTransportNote,
        load.CarrierLoadPurchase,
        sale.SaleReport,
        module='carrier_load', type_='report')
    Pool.register(
        load.PrintLoadOrderShipment,
        stock.LoadShipment,
        load.DoLoadOrder,
        load.CarrierDefine,
        load.PrintCarrierLoadPurchase,
        sale.PrintCarrierNote,
        module='carrier_load', type_='wizard')
