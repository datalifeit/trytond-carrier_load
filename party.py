# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.pyson import Eval


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    cmr_template = fields.Many2One('carrier.load.cmr.template', 'CMR Template',
        states={'readonly': ~Eval('active')})

    @classmethod
    def __register__(cls, module_name):
        table = cls.__table_handler__(module_name)

        cmr_instructions = table.column_exist('cmr_instructions')
        super().__register__(module_name)

        if cmr_instructions:
            table.drop_column('cmr_instructions')
            table.drop_column('carrier_cmr_comment')
