# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Id
from trytond.transaction import Transaction
from trytond.modules.company.model import CompanyValueMixin
from trytond.tools.multivalue import migrate_property
from sql import Literal


config_names = ['load_sequence', 'load_order_sequence']


def default_sequence(name):
    @classmethod
    def default(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('carrier_load', name)
        except KeyError:
            return None
    return default


def default_func(field_name):
    @classmethod
    def default(cls, **pattern):
        return getattr(
            cls.multivalue_model(field_name),
            'default_%s' % field_name, lambda: None)()
    return default


def get_states_model(model):
    pool = Pool()
    Model = pool.get(model)
    states = Model.fields_get(['state'])['state']['selection']
    return [state for state in states if state[0] != 'cancelled']


class Configuration(metaclass=PoolMeta):
    __name__ = 'carrier.configuration'

    load_sequence = fields.MultiValue(
        fields.Many2One('ir.sequence', 'Load Sequence', required=True,
            domain=[
                ('company', 'in',
                    [Eval('context', {}).get('company', -1), None]),
                ('sequence_type', '=', Id('carrier_load',
                    'sequence_type_load'))
            ]))
    load_order_sequence = fields.MultiValue(
        fields.Many2One('ir.sequence', 'Load order Sequence', required=True,
            domain=[
                ('company', 'in',
                    [Eval('context', {}).get('company', -1), None]),
                ('sequence_type', '=', Id('carrier_load',
                    'sequence_type_load_order'))
            ]))
    load_purchasable = fields.Boolean('Load purchasable')
    cmr_template = fields.Many2One('carrier.load.cmr.template', 'CMR Template')
    cmr_copies = fields.Integer('CMR copies', required=True)
    road_note_instructions = fields.Text('Road note instructions', translate=True)
    road_note_copies = fields.Integer('Road note copies', required=True)
    road_note_header = fields.Text('Road note header', translate=True)
    road_note_footer = fields.Text('Road note footer', translate=True)
    vehicle_required = fields.Boolean('Vehicle required')
    trailer_required = fields.Boolean('Trailer required')
    shipment_internal_state = fields.MultiValue(
        fields.Selection('get_shipment_internal_state',
            "Shipment Internal State", sort=False))
    shipment_out_state = fields.MultiValue(
        fields.Selection('get_shipment_out_state', "Shipment Out State",
            sort=False))
    sale_state = fields.MultiValue(
        fields.Selection('get_sale_states', "Sale State", sort=False))

    @classmethod
    def __register__(cls, module_name):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        table = cls.__table_handler__(module_name)
        sql_table = cls.__table__()
        CMRTemplate = pool.get('carrier.load.cmr.template')
        CMRTemplateLine = pool.get('carrier.load.cmr.template.line')
        cmr_template = CMRTemplate.__table__()
        cmr_tline = CMRTemplateLine.__table__()

        cmr_instructions = table.column_exist('cmr_instructions')

        super().__register__(module_name)

        # From 5.0 create default cmr template
        if cmr_instructions:
            cursor.execute(*cmr_template.insert(columns=[
                        cmr_template.create_date,
                        cmr_template.write_date,
                        cmr_template.create_uid,
                        cmr_template.write_uid,
                        cmr_template.name,
                        cmr_template.active],
                    values=sql_table.select(
                        sql_table.create_date,
                        sql_table.write_date,
                        sql_table.create_uid,
                        sql_table.write_uid,
                        Literal('(Default)'),
                        Literal(True))))
            cursor.execute(*cmr_template.select(
                cmr_template.id))
            template = cursor.fetchone()
            if template:
                template, = template
                cursor.execute(*cmr_tline.insert(columns=[
                        cmr_tline.create_date,
                        cmr_tline.write_date,
                        cmr_tline.create_uid,
                        cmr_tline.write_uid,
                        cmr_tline.template,
                        cmr_tline.section,
                        cmr_tline.text],
                    values=sql_table.select(
                        sql_table.create_date,
                        sql_table.write_date,
                        sql_table.create_uid,
                        sql_table.write_uid,
                        Literal(template),
                        Literal('13'),
                        sql_table.cmr_instructions)))
                cursor.execute(*cmr_tline.insert(columns=[
                        cmr_tline.create_date,
                        cmr_tline.write_date,
                        cmr_tline.create_uid,
                        cmr_tline.write_uid,
                        cmr_tline.template,
                        cmr_tline.section,
                        cmr_tline.text],
                    values=sql_table.select(
                        sql_table.create_date,
                        sql_table.write_date,
                        sql_table.create_uid,
                        sql_table.write_uid,
                        Literal(template),
                        Literal('head-right'),
                        Literal('${record.shipment and record.shipment.number}'
                ))))
                cursor.execute(*sql_table.update(
                    [sql_table.cmr_template], [template]))
            table.drop_column('cmr_instructions')

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field in config_names:
            return pool.get('carrier.configuration.sequence')

        fields_states = ['shipment_internal_state', 'shipment_out_state',
            'sale_state']
        if field in fields_states:
            return pool.get('carrier.configuration.states')

        return super(Configuration, cls).multivalue_model(field)

    @classmethod
    def default_load_purchasable(cls):
        return False

    @staticmethod
    def default_cmr_copies():
        return 3

    @staticmethod
    def default_road_note_copies():
        return 3

    default_load_sequence = default_func('load_sequence')
    default_load_order_sequence = default_func('load_order_sequence')

    @staticmethod
    def default_vehicle_required():
        return True

    @staticmethod
    def default_trailer_required():
        return False

    @classmethod
    def get_shipment_internal_state(cls):
        pool = Pool()
        States = pool.get('carrier.configuration.states')
        return States.get_shipment_internal_state()

    @classmethod
    def get_shipment_out_state(cls):
        pool = Pool()
        States = pool.get('carrier.configuration.states')
        return States.get_shipment_out_state()

    @classmethod
    def get_sale_states(cls):
        pool = Pool()
        States = pool.get('carrier.configuration.states')
        return States.get_sale_states()

    default_shipment_internal_state = default_func('shipment_internal_state')
    default_shipment_out_state = default_func('shipment_out_state')
    default_sale_state = default_func('sale_state')


class ConfigurationSequence(ModelSQL, CompanyValueMixin):
    '''Carrier Configuration Sequence'''
    __name__ = 'carrier.configuration.sequence'

    load_sequence = fields.Many2One('ir.sequence', 'Load Sequence',
        domain=[
            ('company', 'in',
                [Eval('context', {}).get('company', -1), None]),
            ('sequence_type', '=', Id('carrier_load',
                'sequence_type_load'))
        ], required=True)
    load_order_sequence = fields.Many2One('ir.sequence', 'Load order Sequence',
        domain=[
            ('company', 'in',
                [Eval('context', {}).get('company', -1), None]),
            ('sequence_type', '=', Id('carrier_load',
                'sequence_type_load_order'))
        ], required=True)

    default_load_sequence = default_sequence('sequence_load')
    default_load_order_sequence = default_sequence('sequence_load_order')

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)
        exist = table_h.table_exist(cls._table)

        super(ConfigurationSequence, cls).__register__(module_name)

        if not exist:
            cls._migrate_property([], [], [])

    @classmethod
    def _migrate_property(cls, field_names, value_names, fields):
        field_names.extend(config_names)
        value_names.extend(config_names)
        fields.append('company')
        migrate_property(
            'carrier.configuration', field_names, cls, value_names)


class ConfigurationStates(ModelSQL, CompanyValueMixin):
    '''Stock Configuration States'''
    __name__ = 'carrier.configuration.states'

    shipment_internal_state = fields.Selection('get_shipment_internal_state',
            "Shipment Internal State", sort=False)
    shipment_out_state = fields.Selection('get_shipment_out_state',
        "Shipment Out State", sort=False)
    sale_state = fields.Selection('get_sale_states', "Sale State", sort=False)

    @staticmethod
    def default_shipment_internal_state():
        return 'done'

    @classmethod
    def get_shipment_internal_state(cls):
        states = get_states_model('stock.shipment.internal')
        return [state for state in states if state[0] != 'request']

    @staticmethod
    def default_shipment_out_state():
        return 'packed'

    @classmethod
    def get_shipment_out_state(cls):
        return get_states_model('stock.shipment.out')

    @staticmethod
    def default_sale_state():
        return 'quotation'

    @classmethod
    def get_sale_states(cls):
        states = get_states_model('sale.sale')
        return [state for state in states if state[0] != 'done']
