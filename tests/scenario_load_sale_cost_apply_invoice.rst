======================
Carrier load sale cost
======================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules, set_user
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import  create_chart, \
    ...     get_accounts, create_tax

Install carrier_load_sale_cost_edit::

    >>> config = activate_modules(['carrier_load', 'sale_cost_apply_invoice'])

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> expense = accounts['expense']
    >>> revenue = accounts['revenue']

Create tax::

    >>> Tax = Model.get('account.tax')
    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()

Create account categories::

    >>> ProductCategory = Model.get('product.category')

    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()

    >>> account_category_tax, = account_category.duplicate()
    >>> account_category_tax.customer_taxes.append(tax)
    >>> account_category_tax.save()

Create parties::

    >>> Party = Model.get('party.party')

    >>> customer = Party(name='Customer')
    >>> customer.save()
    >>> party  = Party(name='Party')
    >>> party.save()

Create carrier product::

    >>> Template = Model.get('product.template')
    >>> Uom = Model.get('product.uom')

    >>> unit, = Uom.find([('name', '=', 'Unit')], limit=1)

    >>> carrier_template = Template()
    >>> carrier_template.name = 'Carrier product'
    >>> carrier_template.type = 'service'
    >>> carrier_template.list_price = Decimal(500)
    >>> carrier_template.cost_price = Decimal(0)
    >>> carrier_template.default_uom = unit
    >>> carrier_template.save()
    >>> carrier_product, = carrier_template.products

Create carrier::

    >>> Carrier = Model.get('carrier')

    >>> party_carrier = Party(name='Carrier 1')
    >>> party_carrier.save()
    >>> party_carrier2 = Party(name='Carrier 2')
    >>> party_carrier2.save()

    >>> carrier = Carrier()
    >>> carrier.party = party_carrier
    >>> carrier.carrier_product = carrier_product
    >>> carrier.save()
    >>> carrier2 = Carrier()
    >>> carrier2.party = party_carrier2
    >>> carrier2.carrier_product = carrier_product
    >>> carrier2.save()

Get warehouse and dock::

    >>> Location = Model.get('stock.location')
    >>> wh, = Location.find([('type', '=', 'warehouse')])

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> Template = Model.get('product.template')

    >>> kg, = ProductUom.find([('name', '=', 'Kilogram')])
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])

    >>> template = Template()
    >>> template.name = 'product 1'
    >>> template.default_uom = kg
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('10')
    >>> template.cost_price = Decimal('5')
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> product, = template.products

    >>> template2 = Template()
    >>> template2.name = 'service'
    >>> template2.default_uom = unit
    >>> template2.type = 'service'
    >>> template2.salable = True
    >>> template2.list_price = Decimal('50')
    >>> template2.cost_price = Decimal('20')
    >>> template2.account_category = account_category_tax
    >>> template2.save()
    >>> service, = template2.products

Create cost types::

    >>> CostType = Model.get('sale.cost.type')
    >>> type_invoice = CostType(name='Service')
    >>> type_invoice.product = service
    >>> type_invoice.formula = 'carrier_amount'
    >>> type_invoice.manual = True
    >>> type_invoice.apply_method = 'invoice_out'
    >>> type_invoice.save()

Create cost templates::

    >>> CostTemplate = Model.get('sale.cost.template')

    >>> template3 = CostTemplate()
    >>> template3.type_ = type_invoice
    >>> template3.save()

Create sale::

    >>> Sale = Model.get('sale.sale')

    >>> gram, = Uom.find([('name', '=', 'Gram')])

    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.invoice_method = 'order'
    >>> sale_line = sale.lines.new()
    >>> sale_line.product = product
    >>> sale_line.quantity = 25.0
    >>> sale_line.unit = gram
    >>> sale.save()

Create load order::

    >>> Load = Model.get('carrier.load')
    >>> Order = Model.get('carrier.load.order')

    >>> load = Load()
    >>> load.warehouse = wh
    >>> load.carrier = carrier
    >>> load.vehicle_number = 'MX1212'
    >>> load.unit_price = Decimal('100')
    >>> load.save()

    >>> load_order = Order(load=load)
    >>> load_order.sale = sale
    >>> load_order.save()

Create cost with invoice out::

    >>> sale.click('quote')
    >>> cost, = sale.costs
    >>> cost.invoice_party == None
    True
    >>> cost.template.apply_method == 'invoice_out'
    True
    >>> cost.delete()
    >>> sale.reload()
    >>> len(sale.costs)
    0
    >>> sale.click('draft')

Create cost with invoice in::

    >>> type_invoice.apply_method = 'invoice_in'
    >>> type_invoice.save()
    >>> sale.click('quote')

    >>> cost, = sale.costs
    >>> cost.invoice_party == party_carrier
    True

Change carrier::

    >>> load.carrier = carrier2
    >>> load.save()
    >>> cost.reload()
    >>> cost.invoice_party == carrier2.party
    True